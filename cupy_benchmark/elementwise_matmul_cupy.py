import numpy as np
import cupy as cp
from cupyx.scipy.ndimage import convolve
import time
import argparse


if __name__ == '__main__':
    # Some technical stuff
    a_gpu = cp.random.rand(10, 10)
    b_gpu = cp.random.rand(10, 10)
    
    print('Start of compiling...')
    _ = product = cp.multiply(a_gpu, b_gpu)
    print('End of compiling.')
    
    num_of_iterations = 100
    
    # First run for N = 10
    N = 10
    times_10 = []
    
    for i in range(num_of_iterations):
        a_gpu = cp.random.rand(N, N)
        b_gpu = cp.random.rand(N, N)
        
        start_time = time.monotonic()
        cp.cuda.stream.get_current_stream().synchronize()
        a = cp.multiply(a_gpu, b_gpu)
        cp.cuda.stream.get_current_stream().synchronize()
        end_time = time.monotonic()
        times_10 += [end_time - start_time]
        
    # First run for N = 100
    N = 100
    times_100 = []
    
    for i in range(num_of_iterations):
        a_gpu = cp.random.rand(N, N)
        b_gpu = cp.random.rand(N, N)
        
        start_time = time.monotonic()
        cp.cuda.stream.get_current_stream().synchronize()
        a = cp.multiply(a_gpu, b_gpu)
        cp.cuda.stream.get_current_stream().synchronize()
        end_time = time.monotonic()
        times_100 += [end_time - start_time]
        
    # First run for N = 1000
    N = 1000
    times_1000 = []
    
    for i in range(num_of_iterations):
        a_gpu = cp.random.rand(N, N)
        b_gpu = cp.random.rand(N, N)
        
        start_time = time.monotonic()
        cp.cuda.stream.get_current_stream().synchronize()
        a = cp.multiply(a_gpu, b_gpu)
        cp.cuda.stream.get_current_stream().synchronize()
        end_time = time.monotonic()
        times_1000 += [end_time - start_time]
        
    # First run for N = 10000
    N = 10000
    times_10000 = []
    
    for i in range(num_of_iterations):
        a_gpu = cp.random.rand(N, N)
        b_gpu = cp.random.rand(N, N)
        
        start_time = time.monotonic()
        a = cp.multiply(a_gpu, b_gpu)
        cp.cuda.stream.get_current_stream().synchronize()
        end_time = time.monotonic()
        times_10000 += [end_time - start_time]
        
    print(f'Size: 10, {np.mean(times_10) * 1000} +/- {np.std(times_10) * 1000}')
    print(f'Size: 100, {np.mean(times_100) * 1000} +/- {np.std(times_100) * 1000}')
    print(f'Size: 1000, {np.mean(times_1000) * 1000} +/- {np.std(times_1000) * 1000}')
    print(f'Size: 10000, {np.mean(times_10000) * 1000} +/- {np.std(times_10000) * 1000}')
    