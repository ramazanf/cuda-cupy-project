#include <stdio.h>
#include <cuda_runtime.h>
#include <math.h>
#include <chrono>
#include <vector>
#include <numeric>
#include <algorithm>


#define CUDA_CHECK(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__global__ void matmul_elementwise(float *d_mat, float *d_vec, float *d_out) {
    int x = blockIdx.x;
    int y = blockIdx.y * blockDim.x + threadIdx.x;
    int idx = x * gridDim.x + y;
    if (y > gridDim.x)
        return;

    d_out[idx] = d_mat[idx] * d_vec[idx]; 
}

void benchmark(int n, int K) {
    float *h_mat = (float *) malloc(n * n * sizeof(float));
    float *h_vec = (float *) malloc(n * n * sizeof(float));
    float *d_mat = 0;
    float *d_vec = 0;
    float *d_out = 0;
    int max_threads = 1024;


    CUDA_CHECK(cudaMalloc((void**)&d_mat, n * n * sizeof(float)));
    CUDA_CHECK(cudaMalloc((void**)&d_vec, n * n * sizeof(float)));
    CUDA_CHECK(cudaMalloc((void**)&d_out, n * n * sizeof(float)));
    
    std::vector<float> times;
    for (int k = 0; k < K; ++k) {
        for (int i = 0; i < n * n; ++i)
            h_mat[i] = (float) rand() / RAND_MAX;

        for (int i = 0; i < n * n; ++i) 
            h_vec[i] = (float) rand() / RAND_MAX;
        
        CUDA_CHECK(cudaMemcpy(d_mat, h_mat, n * n * sizeof(float), cudaMemcpyHostToDevice));
        CUDA_CHECK(cudaMemcpy(d_vec, h_vec, n * n * sizeof(float), cudaMemcpyHostToDevice));

        CUDA_CHECK(cudaMemset(d_out, 0, n * n * sizeof(float)));

        auto start = std::chrono::high_resolution_clock::now();

        int num_blocks = n / max_threads + ((n % max_threads) != 0);
        matmul_elementwise<<<dim3(n, num_blocks, 1), max_threads>>>(d_mat, d_vec, d_out);
        CUDA_CHECK(cudaDeviceSynchronize());
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<float, std::milli> duration = end - start;

        times.push_back(duration.count());
    }

    float mean = std::accumulate(times.begin(), times.end(), 0.0) / times.size();
    float sq_sum = std::inner_product(times.begin(), times.end(), times.begin(), 0.0);
    float stddev = std::sqrt(sq_sum / times.size() - mean * mean);

    printf("Size: %d, Mean time: %f ms, Stddev: %f ms; %.3f +/- %.3f %.3f\n", n, mean, stddev, mean, stddev, times[times.size() - 1]);

    free(h_mat);
    free(h_vec);
    CUDA_CHECK(cudaFree(d_mat));
    CUDA_CHECK(cudaFree(d_vec));
    CUDA_CHECK(cudaFree(d_out));
}

int main() {
    int K = 10;

    int sizes[] = {10, 100, 1000, 10000};
    for (int size : sizes) {
        benchmark(size, K);
    }

    return 0;
}