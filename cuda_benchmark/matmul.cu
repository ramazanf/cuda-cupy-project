#include <stdio.h>
#include <cuda_runtime.h>
#include <math.h>
#include <chrono>
#include <vector>
#include <numeric>
#include <algorithm>


#define CUDA_CHECK(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__global__ void _matmul(float *d_mat, float *d_vec, float *d_out, int res, int m, int k) {
    extern __shared__ float cache[];

    int tid = threadIdx.x;
    int cols_per_block = blockDim.x;
    int blocks_per_row = gridDim.y;

    int x = blockIdx.x;
    int y = blockIdx.y * cols_per_block + tid;
    int out_col = blockIdx.z;

    if (blockIdx.y == gridDim.y - 1)
        cols_per_block = res;

    float val = 0;

    // skip rest of the useless threads
    if (!(blockIdx.y == gridDim.y - 1 && tid > res))
        val = d_mat[x * m + y] * d_vec[y * k + out_col];

    cache[tid] = val;
    __syncthreads();

    for (int i = (cols_per_block + 1) / 2; i > 0; i = (i + 1) / 2) {
        if (tid < i && tid + i < cols_per_block) {
            cache[tid] += cache[tid + i];
        }
        __syncthreads();
        if (i == 1) break;
    }

    if (tid == 0) {
        if (cols_per_block % 2 == 1)
            cache[0] += cache[cols_per_block - 1];
        d_out[out_col * gridDim.x * blocks_per_row + x * blocks_per_row + blockIdx.y] = cache[0];
    }
}

__global__ void _reduce_matmul(float *d_out, const float *d_in) {
    extern __shared__ float cache[];

    int row = blockIdx.x;
    int cols = blockDim.x;
    int tid = threadIdx.x; 
    int out_col = blockDim.y;

    cache[tid] = d_in[out_col * gridDim.x * cols + row * cols + tid];
    __syncthreads();

    for (int i = (cols + 1) / 2; i > 0; i = (i + 1) / 2) {
        if (tid < i && tid + i < cols) {
            cache[tid] += cache[tid + i];
        }
        __syncthreads();
        if (i == 1) break;
    }

    if (tid == 0) {
        if (cols % 2 == 1)
            cache[0] += cache[cols - 1];
        
        d_out[row * gridDim.y + out_col] = cache[0];
    }
}

__host__ void matmul(float *d_mat, float *d_vec, float *d_out, int n, int m, int k, int max_threads) {
    int blocks_per_row = m / max_threads + (m % max_threads != 0);
    int res = m % max_threads;

    float *d_temp;
    CUDA_CHECK(cudaMalloc((void**)&d_temp, n * blocks_per_row * k * sizeof(float)));

    _matmul<<<dim3(n, blocks_per_row, k), max_threads, max_threads * sizeof(float)>>>(d_mat, d_vec, d_temp, res, m, k);

    CUDA_CHECK(cudaDeviceSynchronize());

    _reduce_matmul<<<dim3(n, k, 1), blocks_per_row, blocks_per_row * sizeof(float)>>>(d_out, d_temp);

    CUDA_CHECK(cudaFree(d_temp));
}

void benchmark(int n, int K) {
    float *h_mat = (float *) malloc(n * n * sizeof(float));
    float *h_vec = (float *) malloc(n * n * sizeof(float));
    float *d_mat = 0;
    float *d_vec = 0;
    float *d_out = 0;
    int max_threads = 1024;



    CUDA_CHECK(cudaMalloc((void**)&d_mat, n * n * sizeof(float)));
    CUDA_CHECK(cudaMalloc((void**)&d_vec, n * n * sizeof(float)));
    CUDA_CHECK(cudaMalloc((void**)&d_out, n * n * sizeof(float)));
    
    std::vector<float> times;
    for (int k = 0; k < K; ++k) {
        for (int i = 0; i < n * n; ++i) {
            h_mat[i] = (float) rand() / RAND_MAX;
        }

        for (int i = 0; i < n * n; ++i) {
            h_vec[i] = (float) rand() / RAND_MAX;
        }

        CUDA_CHECK(cudaMemcpy(d_mat, h_mat, n * n * sizeof(float), cudaMemcpyHostToDevice));
        CUDA_CHECK(cudaMemcpy(d_vec, h_vec, n * n * sizeof(float), cudaMemcpyHostToDevice));

        CUDA_CHECK(cudaMemset(d_out, 0, n * n * sizeof(float)));

        auto start = std::chrono::high_resolution_clock::now();
        matmul(d_mat, d_vec, d_out, n, n, n, max_threads);
        CUDA_CHECK(cudaDeviceSynchronize());
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<float, std::milli> duration = end - start;

        times.push_back(duration.count());
    }

    float mean = std::accumulate(times.begin(), times.end(), 0.0) / times.size();
    float sq_sum = std::inner_product(times.begin(), times.end(), times.begin(), 0.0);
    float stddev = std::sqrt(sq_sum / times.size() - mean * mean);

    printf("Size: %d, Mean time: %f ms, Stddev: %f ms; %.3f +/- %.3f %.3f\n", n, mean, stddev, mean, stddev, times[times.size() - 1]);

    free(h_mat);
    free(h_vec);
    CUDA_CHECK(cudaFree(d_mat));
    CUDA_CHECK(cudaFree(d_vec));
    CUDA_CHECK(cudaFree(d_out));
}

int main() {
    int K = 1;

    int sizes[] = {10, 100, 1000, 10000};
    for (int size : sizes) {
        benchmark(size, K);
    }

    return 0;
}