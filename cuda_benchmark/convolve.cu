#include <stdio.h>
#include <cuda_runtime.h>
#include <math.h>
#include <chrono>
#include <vector>
#include <numeric>
#include <algorithm>


#define CUDA_CHECK(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__global__ void init_gaussian_kernel(float *kernel, int ksize, float sigma) {
    int i = threadIdx.x;
    int j = threadIdx.y;
    __shared__ float scale;
    scale = 0;
    __syncthreads();

    int si = i - (ksize - 1) / 2;
    int sj = j - (ksize - 1) / 2;
    float val = exp(-(si * si + sj * sj)/(2 * sigma * sigma));

    atomicAdd(&scale, val);
    __syncthreads();
    kernel[i*ksize + j] = val / scale;
}

__global__ void init_average_kernel(float *kernel, int ksize) {
    int i = threadIdx.x;
    int j = threadIdx.y;

    kernel[i*ksize + j] = 1 / (float) (ksize * ksize);
}

__global__ void convolve(float *d_in, float *d_out, float *kernel) {
    // extern __shared__ float cache[];

    int x_out = blockIdx.x;
    int y_out = blockIdx.y;
    int c_out = blockIdx.z;        

    int x_kernel = threadIdx.x;
    int y_kernel = threadIdx.y;
    
    int ksize = blockDim.x;
    int tid = threadIdx.x * ksize + threadIdx.y;

    int x_shift = x_kernel - (ksize - 1) / 2;
    int y_shift = y_kernel - (ksize - 1) / 2;

    int x_in = x_out + (ksize - 1) / 2 + x_shift;
    int y_in = y_out + (ksize - 1) / 2 + y_shift;

    int w = gridDim.y + (ksize - 1);

    int in_idx = (x_in * w + y_in) * gridDim.z + c_out;
    float val = d_in[in_idx] * kernel[x_kernel * ksize + y_kernel];
    int out_idx = (x_out * gridDim.y + y_out) * gridDim.z + c_out;

    atomicAdd(&d_out[out_idx], val);

    // cache[tid] = val;
    // __syncthreads();

    // int els = ksize * ksize;
    // for (int i = (els + 1) / 2; i > 0; i = (i + 1) / 2) {
    //     if (tid < i && tid + i < els) {
    //         cache[tid] += cache[tid + i];
    //     }
    //     if (i == 1) break;
    //     __syncthreads();
    // }
    
    // if (tid == 0)
    //     d_out[out_idx] = cache[0];
}

void benchmark_convolution(int size, int ksize, int K) {
    int tensor_size = size * size;
    int out_n = (size - (ksize - 1));
    int out_size = out_n * out_n; 

    float *h_tensor = (float *) malloc(tensor_size * sizeof(float));
    float *h_out = (float *) malloc(out_size * sizeof(float));
    float *d_tensor = 0;
    float *d_out = 0;
    float *kernel = 0;



    CUDA_CHECK(cudaMalloc((void**)&d_tensor, tensor_size * sizeof(float)));
    CUDA_CHECK(cudaMalloc((void**)&d_out, tensor_size * sizeof(float)));
    CUDA_CHECK(cudaMalloc((void**)&kernel, ksize * ksize * sizeof(float)));

    float sigma = 0.3 * ((ksize-1) * 0.5 - 1) + 0.8; // opencv init
    init_gaussian_kernel<<<1, dim3(ksize, ksize, 1)>>>(kernel, ksize, sigma);
 
    std::vector<float> times;
    for (int k = 0; k < K; ++k) {
        for (int i = 0; i < tensor_size; ++i) {
            h_tensor[i] = (float) rand() / RAND_MAX;
        }
        CUDA_CHECK(cudaMemcpy(d_tensor, h_tensor, tensor_size * sizeof(float), cudaMemcpyHostToDevice));
    
        CUDA_CHECK(cudaMemset(d_out, 0, tensor_size * sizeof(float)));

        auto start = std::chrono::high_resolution_clock::now();
        convolve<<<dim3(out_n, out_n, 1), dim3(ksize, ksize, 1)>>>(d_tensor, d_out, kernel);
        // convolve<<<dim3(out_n, out_n, 1), dim3(ksize, ksize, 1), ksize * ksize * sizeof(float)>>>(d_tensor, d_out, kernel);
        CUDA_CHECK(cudaGetLastError());
        CUDA_CHECK(cudaDeviceSynchronize());
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<float, std::milli> duration = end - start;

        times.push_back(duration.count());
    }

    float mean = std::accumulate(times.begin(), times.end(), 0.0) / times.size();
    float sq_sum = std::inner_product(times.begin(), times.end(), times.begin(), 0.0);
    float stddev = std::sqrt(sq_sum / times.size() - mean * mean);

    printf("Size: %d, Mean time: %f ms, Stddev: %f ms; %.3f +/- %.3f\n", size, mean, stddev, mean, stddev);

    free(h_tensor);
    free(h_out);
    CUDA_CHECK(cudaFree(d_tensor));
    CUDA_CHECK(cudaFree(d_out));
    CUDA_CHECK(cudaFree(kernel));
}

int main() {
    int K = 10;
    int ksize = 3;

    int sizes[] = {10, 100, 1000, 10000};
    for (int size : sizes) {
        benchmark_convolution(size, ksize, K);
    }

    return 0;
}