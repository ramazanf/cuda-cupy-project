#include <stdio.h>
#include <cuda_runtime.h>
#include <math.h>
#include <chrono>
#include <vector>
#include <numeric>
#include <algorithm>
#include <cuComplex.h>



#define CUDA_CHECK(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__global__ void bitReversalKernel(cuFloatComplex* data, int n) {
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    if (tid >= n) return;

    int j = 0;
    for (int i = 1, bit = n >> 1; bit; i <<= 1, bit >>= 1) {
        if (tid & i) j |= bit;
    }

    if (tid < j) {
        cuFloatComplex temp = data[tid];
        data[tid] = data[j];
        data[j] = temp;
    }
}

__global__ void fftKernel(cuFloatComplex* data, int n, int step) {
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    int halfStep = step / 2;
    if (tid >= n / step) return;

    int k = tid * step;
    cuFloatComplex w = make_cuFloatComplex(1.0f, 0.0f);
    float angle = -2.0f * M_PI / step;
    cuFloatComplex wStep = make_cuFloatComplex(cosf(angle), sinf(angle));

    for (int j = 0; j < halfStep; ++j) {
        cuFloatComplex t = cuCmulf(w, data[k + j + halfStep]);
        data[k + j + halfStep] = cuCsubf(data[k + j], t);
        data[k + j] = cuCaddf(data[k + j], t);
        w = cuCmulf(w, wStep);
    }
}

void fft(cuFloatComplex* data, int n) {
    cudaDeviceProp prop;
    int device;
    cudaGetDevice(&device);
    cudaGetDeviceProperties(&prop, device);
    int blockSize = prop.maxThreadsPerBlock;
    if (blockSize >= n) { blockSize = n; }
    int gridSize = (n + blockSize - 1) / blockSize;
    
    // Bit reversal permutation
    bitReversalKernel<<<gridSize, blockSize>>>(data, n);
    CUDA_CHECK(cudaPeekAtLastError());
    CUDA_CHECK(cudaDeviceSynchronize());

    // FFT computation
    for (int step = 2; step <= n; step <<= 1) {
        gridSize = (n / step + blockSize - 1) / blockSize;
        fftKernel<<<gridSize, blockSize>>>(data, n, step);
        CUDA_CHECK(cudaPeekAtLastError());
        CUDA_CHECK(cudaDeviceSynchronize());
    }
}

void benchmark(int n, int K) {
    cuFloatComplex *h_vec = (cuFloatComplex *) malloc(n * sizeof(cuFloatComplex));
    cuFloatComplex *d_vec = 0;
    float *d_out = 0;

    CUDA_CHECK(cudaMalloc((void**)&d_vec, n * sizeof(cuFloatComplex)));
    CUDA_CHECK(cudaMalloc((void**)&d_out, n * sizeof(float)));


    std::vector<float> times;
    for (int k = 0; k < K; ++k) {
        for (int i = 0; i < n; ++i) {
            h_vec[i] = make_cuFloatComplex((float) rand() / RAND_MAX, (float) rand() / RAND_MAX);
        }

        CUDA_CHECK(cudaMemcpy(d_vec, h_vec, n * sizeof(float), cudaMemcpyHostToDevice));
    

        CUDA_CHECK(cudaMemset(d_out, 0, n * sizeof(float)));

        auto start = std::chrono::high_resolution_clock::now();
        fft(d_vec, n);
        CUDA_CHECK(cudaDeviceSynchronize());
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<float, std::milli> duration = end - start;

        times.push_back(duration.count());
    }

    float mean = std::accumulate(times.begin(), times.end(), 0.0) / times.size();
    float sq_sum = std::inner_product(times.begin(), times.end(), times.begin(), 0.0);
    float stddev = std::sqrt(sq_sum / times.size() - mean * mean);

    printf("Size: %d, Mean time: %f ms, Stddev: %f ms; %.3f +/- %.3f %.3f\n", n, mean, stddev, mean, stddev, times[times.size() - 1]);

    free(h_vec);
    CUDA_CHECK(cudaFree(d_vec));
    CUDA_CHECK(cudaFree(d_out));
}

int main() {
    int K = 10;

    int sizes[] = {10, 100, 1000, 10000};
    for (int size : sizes) {
        benchmark(size, K);
    }

    return 0;
}